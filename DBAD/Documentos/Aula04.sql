CREATE TABLE CLIENTE_ERP
AS SELECT * FROM pf0064.cliente_erp;


select * from  cliente_erp;

SET SERVEROUTPUT ON
--Cursor Implicito
DECLARE
        v_cod_cli   number;
        v_nome      varchar2(100);
BEGIN
    SELECT CD_CLIENTE, NM_CLIENTE
    INTO V_COD_CLI, V_NOME
    FROM CLIENTE_ERP
    WHERE CD_CLIENTE = 1;
    DBMS_OUTPUT.PUT_LINE('Cod=> ' || v_cod_cli ||
                        ' Nome=> ' || v_nome);
END;
--Recebendo tipo de acordo com a estrutura da Tabela
DECLARE
        v_cod_cli   cliente_erp.cd_cliente%type;
        v_nome      cliente_erp.nm_cliente%type;
BEGIN
    SELECT CD_CLIENTE, NM_CLIENTE
    INTO V_COD_CLI, V_NOME
    FROM CLIENTE_ERP
    WHERE CD_CLIENTE = 1;
    DBMS_OUTPUT.PUT_LINE('Cod=> ' || v_cod_cli ||
                        ' Nome=> ' || v_nome);
END;



DECLARE
    v_registro  cliente_erp%rowtype;
BEGIN
    select * 
    into v_registro
    from cliente_erp
    where cd_cliente =1;
    
    DBMS_OUTPUT.PUT_LINE(v_registro.cd_cliente || ' ' || v_registro.nm_cliente);
END;



--ROW Type
--1) Desenvolver um bloco PL/SQL quer armazene o resultado do comando select abaixo e imprima os valores utilizando o dbms_output.
--Comando: SELECT USER, SYSDATE FROM DUAL;

--2) Desenvolver um bloco PL/SQL que realize a sumarização da pontuação dos cliente existentes na tabela pf0064.cliente_erp.
/*
DECLARE 
    v_soma number;
BEGIN

END;
*/
--3) Desenvolver um bloco  PL/SQL que retorne a quantidade de caracteres ocupados pelos dados da tabela pf0064.cliente_erp.
