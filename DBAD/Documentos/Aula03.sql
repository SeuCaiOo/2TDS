set SERVEROUTPUT ON;
--1) Desenvolver um bloco PL/SQL que exiba o nome e a idade armazenados em vari�veis.
DECLARE 
    v_nome VARCHAR(100);
    v_idade INTEGER;
BEGIN
    v_nome:= 'Caio';
    v_idade:= 21;
DBMS_OUTPUT.PUT_LINE('Nome: '|| v_nome || '. ' || v_idade || ' anos.' );
END;
    
--2) Desenvolver um bloco PL/SQL que receba 5 valores e armazene a m�dia dos 5 valores. Armazenar somente n�meros inteiros.
DECLARE
    v_n1 NUMBER(2,1);
    v_n2 NUMBER(2,1);
    v_n3 NUMBER(2,1);
    v_n4 NUMBER(2,1);
    v_n5 NUMBER(2,1);
    v_media NUMBER;
BEGIN
v_n1:= 1.2; v_n2:=9.3; v_n3:=5.1; v_n4:=6.2; v_n5:=3.1; 
--v_media:= (v_n1 + v_n2 + v_n3 + v_n4 + v_n5)/5;
    v_media:= TRUNC((v_n1 + v_n2 + v_n3 + v_n4 + v_n5)/5);
    -- v_media:= ROUND((v_n1 + v_n2 + v_n3 + v_n4 + v_n5)/5);
DBMS_OUTPUT.PUT_LINE(v_media);
END;

--3) Desenvolver um bloco PL/SQL que receba o nome completo e armazene somente o primeiro nome.
DECLARE
    v_nomecompleto VARCHAR2(200);
    v_nome VARCHAR(100);
BEGIN
    v_nomecompleto:= 'Caio pimentel alves da silva';   
    v_nome:= SUBSTR( v_nomecompleto ,1,INSTR(v_nomecompleto, ' '));
    DBMS_OUTPUT.PUT_LINE('Primeiro nome: '|| v_nome);
END;

--4) Desenvolver um bloco PL/SQL que receba um nome completo em letras min�sculas e imprima todos os nomes com a primeira letra em mai�sculo.
DECLARE
    v_nome VARCHAR2(200);
BEGIN
    v_nome:= 'caio pimentel alves da silva';   
DBMS_OUTPUT.PUT_LINE(INITCAP(v_nome));    
END;
--5) Desenvolver um bloco PL/SQL que receba uma data no formato 'dd/mm/yyyy' e imprima o dia da semana.
DECLARE
    v_data DATE;
BEGIN
    v_data := TO_DATE('01/01/2018', 'dd/mm/yyyy');
    
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(v_data, 'day'));
END;
