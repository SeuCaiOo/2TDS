SELECT * FROM LOC_CIDADE;

SELECT * FROM LOC_ESTADO;

--Desenvolver um bloco PL/SQL que imprima os dados de clientes da tabela LOC_CLIENTE.
--Obs: retornar somente clientes do tipo pessoa fisica.

SET SERVEROUTPUT ON

DECLARE 
    cursor c_cliente is 
        select * from loc_cliente
            where tp_cliente = 'F';
            
    v_cliente  c_cliente%rowtype;

BEGIN 
    open c_cliente;
    loop
        fetch c_cliente into v_cliente;
        exit when c_cliente%notfound;
        DBMS_OUTPUT.PUT_LINE(v_cliente.nm_cliente || ' - ' || v_cliente.tp_cliente );
    end loop;
    close c_cliente;
END;