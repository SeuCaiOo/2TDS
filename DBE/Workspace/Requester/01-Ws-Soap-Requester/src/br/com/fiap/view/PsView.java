package br.com.fiap.view;

import java.rmi.RemoteException;
import java.util.Scanner;

import org.apache.axis2.AxisFault;

import br.com.fiap.bo.NotaBOStub;
import br.com.fiap.bo.NotaBOStub.CalcularPs;
import br.com.fiap.bo.NotaBOStub.CalcularPsResponse;

public class PsView {

	public static void main(String[] args) {
		
		//Chamar o ws para calcular a nota  de ps necess�ria
		Scanner sc = new Scanner(System.in);
		
		try {
			NotaBOStub stub = new NotaBOStub();
			CalcularPs parametros = new CalcularPs();
			
			System.out.println("Informe sua nota de NAC: ");
			parametros.setNac(sc.nextFloat());
			System.out.println("Informe sua nota de AM: ");
			parametros.setAm(sc.nextFloat());
			
		//	parametros.setAm(7);
		//	parametros.setNac(8);
			
			CalcularPsResponse resp = stub.calcularPs(parametros);	
			
			System.out.println("Voc� necessita tirar: " + resp.get_return() + " na PS.");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		sc.close();
	

	}

}
