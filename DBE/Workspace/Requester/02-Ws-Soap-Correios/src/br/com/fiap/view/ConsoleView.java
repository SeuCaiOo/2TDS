package br.com.fiap.view;

import java.rmi.RemoteException;
import java.util.Scanner;

import org.apache.axis2.AxisFault;
import org.tempuri.CalcPrecoPrazoWSStub;
import org.tempuri.CalcPrecoPrazoWSStub.CServico;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazo;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazoResponse;

public class ConsoleView {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		
		try {
			CalcPrecoPrazoWSStub stup = new CalcPrecoPrazoWSStub();
			
			CalcPrazo params = new CalcPrazo();
			System.out.println("Informe o N� do Servi�o: ");
			params.setNCdServico(sc.nextLine());
			System.out.println("Informe o CEP de origem: ");
			params.setSCepOrigem(sc.next()+sc.nextLine());
			System.out.println("Informe o CEP de destino: ");
			params.setSCepDestino(sc.next()+sc.nextLine());
			
			CalcPrazoResponse response = stup.calcPrazo(params);
			
			CServico servico = response.getCalcPrazoResult().getServicos().getCServico()[0];
			
			System.out.println("A entrega chegar� em " + 
			servico.getPrazoEntrega()+
			" dias.");
			System.out.println("Data m�xima: " + 
					servico.getDataMaxEntrega()+
					".");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sc.close();

	}

}
