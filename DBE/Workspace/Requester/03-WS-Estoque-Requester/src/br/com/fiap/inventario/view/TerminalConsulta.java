package br.com.fiap.inventario.view;

import java.util.Scanner;

import br.com.fiap.inventario.bo.EstoqueBOStub;
import br.com.fiap.inventario.bo.EstoqueBOStub.ConsultarProduto;
import br.com.fiap.inventario.bo.EstoqueBOStub.ConsultarProdutoResponse;
import br.com.fiap.inventario.bo.EstoqueBOStub.ProdutoTO;

public class TerminalConsulta {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		try {
			EstoqueBOStub stub = new EstoqueBOStub();
			ConsultarProduto params = new ConsultarProduto();
			System.out.println("Digite o c�digo: ");
			params.setCodigo(sc.nextInt());
			
			ConsultarProdutoResponse resp = stub.consultarProduto(params);
			
			ProdutoTO produto = resp.get_return();
			System.out.println(produto.getNome());
			System.out.println(produto.getPrecoUnitario());
			System.out.println(produto.getQuantidaEstoque());

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
		
		
		
		sc.close();

	}

}
