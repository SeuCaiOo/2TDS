package br.com.fiap.inventario.view;

import java.util.*;

import br.com.fiap.inventario.bo.EstoqueBOStub;
import br.com.fiap.inventario.bo.EstoqueBOStub.Listar;
import br.com.fiap.inventario.bo.EstoqueBOStub.ListarResponse;
import br.com.fiap.inventario.bo.EstoqueBOStub.ProdutoTO;

public class ListarView {

	public static void main(String[] args) {
		
		try {
			EstoqueBOStub stub = new EstoqueBOStub();
			Listar params = new Listar();
			ListarResponse resp = stub.listar(params);
			
			ProdutoTO[] array = resp.get_return();
			
			//Transformar o array em lista
			List<ProdutoTO> lista = Arrays.asList(array);
			for (ProdutoTO p : lista) {
				System.out.println(p.getNome());
				System.out.println(p.getPrecoUnitario());
				System.out.println(p.getQuantidaEstoque());
				System.out.println("***********");
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		

	}

}
