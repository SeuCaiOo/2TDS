package br.com.fiap.ws.view;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import br.com.fiap.inventario.bo.EstoqueBOStub.ProdutoTO;
import br.com.fiap.service.ProdutoService;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class TelaProduto {

	protected Shell shell;
	private Text txtCodigo;
	private Text txtQuantidade;
	private Text txtProduto;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TelaProduto window = new TelaProduto();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblCodigo = new Label(shell, SWT.NONE);
		lblCodigo.setBounds(10, 37, 130, 15);
		lblCodigo.setText("C\u00F3digo do Produto: ");
		
		Label lblProduto = new Label(shell, SWT.NONE);
		lblProduto.setBounds(58, 85, 55, 15);
		lblProduto.setText("Produto:");
		
		Label lblQuantidade = new Label(shell, SWT.NONE);
		lblQuantidade.setBounds(46, 120, 76, 15);
		lblQuantidade.setText("Quantidade:");
		
		txtCodigo = new Text(shell, SWT.BORDER);
		txtCodigo.setBounds(159, 34, 148, 21);
		
		Button btnPesquisar = new Button(shell, SWT.NONE);
		btnPesquisar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int codigo = Integer.parseInt(txtCodigo.getText());
				
				try {
					ProdutoService service = new ProdutoService();
					ProdutoTO prd = service.consultarProduto(codigo);
					txtProduto.setText(prd.getNome());
					
				} catch (Exception e1) {
					lblProduto.setText(e1.getMessage());
				}
			}
		});
		btnPesquisar.setBounds(317, 32, 75, 25);
		btnPesquisar.setText("Pesquisar");
		
		txtQuantidade = new Text(shell, SWT.BORDER);
		txtQuantidade.setBounds(159, 114, 148, 21);
		
		txtProduto = new Text(shell, SWT.BORDER);
		txtProduto.setBounds(159, 73, 148, 21);

	}

}
