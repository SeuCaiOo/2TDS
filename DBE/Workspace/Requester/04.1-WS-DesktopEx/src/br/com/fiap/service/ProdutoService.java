package br.com.fiap.service;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import br.com.fiap.inventario.bo.EstoqueBOStub;
import br.com.fiap.inventario.bo.EstoqueBOStub.ConsultarProduto;
import br.com.fiap.inventario.bo.EstoqueBOStub.ConsultarProdutoResponse;
import br.com.fiap.inventario.bo.EstoqueBOStub.ProdutoTO;

public class ProdutoService {

	private EstoqueBOStub stub;
	
	public ProdutoService() throws AxisFault{
		stub = new EstoqueBOStub();
	}
	
	public ProdutoTO consultarProduto(int codigo) throws RemoteException {
		ConsultarProduto idProduto = new ConsultarProduto();
		idProduto.setCodigo(codigo);
		
		ConsultarProdutoResponse resp = stub.consultarProduto(idProduto);
		 
		return resp.get_return();
	}
	
}
