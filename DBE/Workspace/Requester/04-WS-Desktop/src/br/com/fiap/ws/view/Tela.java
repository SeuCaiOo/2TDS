package br.com.fiap.ws.view;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import br.com.fiap.service.NotaService;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class Tela {

	protected Shell shell;
	private Text txtNAC;
	private Text txtAM;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Tela window = new Tela();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblNac = new Label(shell, SWT.NONE);
		lblNac.setBounds(10, 25, 55, 15);
		lblNac.setText("NAC");
		
		Label lblAm = new Label(shell, SWT.NONE);
		lblAm.setBounds(10, 62, 55, 15);
		lblAm.setText("AM");
		
		txtNAC = new Text(shell, SWT.BORDER);
		txtNAC.setBounds(100, 22, 76, 21);
		
		txtAM = new Text(shell, SWT.BORDER);
		txtAM.setBounds(100, 59, 76, 21);
		

		Label lblResultado = new Label(shell, SWT.NONE);
		lblResultado.setBounds(34, 144, 223, 15);
		
		Button btnCalcular = new Button(shell, SWT.NONE);
		btnCalcular.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//Clique do Bot�o
				float am = Float.parseFloat(txtAM.getText());
				float nac = Float.parseFloat(txtNAC.getText());
				try {
					NotaService service = new NotaService();
					float ps = service.calcularPs(am, nac);
					lblResultado.setText(String.valueOf(ps));
				} catch (Exception e1) {
					lblResultado.setText(e1.getMessage());
				}
				
			}
		});
		btnCalcular.setBounds(101, 113, 75, 25);
		btnCalcular.setText("Calcular");
		

	}
}
