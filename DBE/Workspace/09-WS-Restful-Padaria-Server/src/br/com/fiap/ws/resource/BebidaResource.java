package br.com.fiap.ws.resource;

import java.util.List;

import javax.persistence.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import br.com.fiap.ws.dao.BebidaDAO;
import br.com.fiap.ws.dao.impl.BebidaDAOImpl;
import br.com.fiap.ws.entity.Bebida;
import br.com.fiap.ws.exception.CommitException;
import br.com.fiap.ws.singleton.EntityManagerFactorySingleton;

@Path("/bebida")
public class BebidaResource {

	private BebidaDAO bebidaDao;
	
	public BebidaResource() {
		EntityManager em = EntityManagerFactorySingleton
				.getInstance().createEntityManager();
		
		bebidaDao = new BebidaDAOImpl(em);
	}
	
	/** Cadastrar Bebida */
	@POST
	//Recebe um JSON para o Metodo
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastrar(Bebida bebida, @Context UriInfo url) {
		
		try {
			bebidaDao.create(bebida);
			bebidaDao.commit();
		} catch (CommitException e) {
			//Retornar o codigo do erro HTTP = 505
			e.printStackTrace();
			return Response.serverError().build();
		}
		//Cria a URL para acessar o recurso criado
		UriBuilder b = url.getAbsolutePathBuilder();
		b.path(String.valueOf(bebida.getCodigo()));
		//201 - Created
		return Response.created(b.build()).build();
		
		
	}
		
	
	/** Atualizar Bebeida */
	@PUT
	//Recebe um JSON para o Metodo
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response ataulizar(Bebida bebida, 
			@PathParam("id") int codigo) {
		try {
			bebida.setCodigo(codigo);
			bebidaDao.update(bebida);
			bebidaDao.commit();
		} catch (CommitException e) {
			//Retornar o codigo do erro HTTP
			e.printStackTrace();
			return Response.serverError().build();
		}
		//200 - OK
		return Response.ok().build();
		
	}
	
	/** Deletar Bebida*/
	@DELETE
	@Path("{id}")
	public void remover(@PathParam("id") int codigo) {
		try {
			bebidaDao.delete(codigo);
			bebidaDao.commit();
		} catch (Exception e) {
			//Exce��o com Status Code 500
			e.printStackTrace();
			throw new WebApplicationException(
					Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	/** Listar uma Bebida pelo C�digo */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Bebida buscar(@PathParam("id") int codigo) {
		return bebidaDao.read(codigo);
	}
	
	
	/** Listar todas Bebidas*/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Bebida> listar() {
		return bebidaDao.list();
	}
	
	
}
