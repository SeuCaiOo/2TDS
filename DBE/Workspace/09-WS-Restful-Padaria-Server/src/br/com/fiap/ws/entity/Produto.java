package br.com.fiap.ws.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "produto", sequenceName = "SEQ_T_PRODUTO", allocationSize =1)
@Table(name = "T_PRODUTO")
public class Produto {
	
	@Id
	@GeneratedValue(generator = "produto", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name = "")
	private String nome;
	
	@Column(name = "")
	private Calendar dataValidade;
	
	@Column(name = "")
	private double preco;
	
	
	

	public Produto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Produto(String nome, Calendar dataValidade, double preco) {
		super();
		this.nome = nome;
		this.dataValidade = dataValidade;
		this.preco = preco;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Calendar dataValidade) {
		this.dataValidade = dataValidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	
	

}
