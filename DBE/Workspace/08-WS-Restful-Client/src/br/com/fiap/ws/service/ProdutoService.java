package br.com.fiap.ws.service;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.fiap.ws.to.Produto;

public class ProdutoService {

	private Client client = Client.create();
	private static final String URL = "http://localhost:8080/07-WS-Restful-Server/rest/produto/";


	public void remover (int codigo) throws Exception {
		//Cria a URL com o c�digo no final
		WebResource w = client.resource (URL).path(String.valueOf(codigo));
		//Chama o WebService
		ClientResponse response = w
				//Tipo de dados que ser�o enviados (JSON)
				.delete(ClientResponse.class);
		//204
		if(response.getStatus() != 204) {
			throw new Exception("Erro " + response.getStatus());
		}
		
	}



	public void atualizar(Produto produto) throws Exception {
		//Cria a URL com o c�digo no final
		WebResource w = client.resource (URL).path(String.valueOf(produto.getCodigo()));
		//Chama o WebService
		ClientResponse response = w
				//Tipo de dados que ser�o enviados (JSON)
				.type(MediaType.APPLICATION_JSON).put(ClientResponse.class, produto);
		//200
		if (response.getStatus() != 200) {
			throw new Exception("Erro " + response.getStatus());
		}

	}


	public void cadastrar(Produto produto) throws Exception {
		//Cria a URL
		WebResource w = client.resource (URL);
		//Chama o WebService
		ClientResponse response = w
				//Tipo de dados que ser�o enviados
				.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, produto);
		//201 Creat
		if(response.getStatus() == 201) {
			//Console
			System.out.println("Cadastrado com Sucesso ! " + response.getLocation());
		} else { 
			throw new Exception("Erro " + response.getStatus());
		}
	}


	public List<Produto> listar() throws Exception {
		//Cria a URL
		WebResource w = client.resource (URL);
		//Chama o WebService
		ClientResponse response = w
				//Recepe JSON
				.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		if(response.getStatus() != 200) {
			throw new Exception("Erro " + response.getStatus());
		}
		//Retorna o produto encontrado
		return Arrays.asList(response.getEntity(Produto[].class));
	}


	public Produto pesquisar(int codigo) throws Exception {
		// Cria a URL com o c�digo no final
		WebResource w = client.resource(URL).path(String.valueOf(codigo));
		// Chama o WebService
		ClientResponse response = w
				// Recepe JSON
				.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		if (response.getStatus() != 200) {
			throw new Exception("Erro " + response.getStatus());
		}
		// Retorna o produto encontrado
		return response.getEntity(Produto.class);
	}

}
