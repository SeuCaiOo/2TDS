package br.com.fiap.ws.resource;

import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import br.com.fiap.ws.dao.ProdutoDAO;
import br.com.fiap.ws.dao.impl.ProdutoDAOImpl;
import br.com.fiap.ws.entity.Produto;
import br.com.fiap.ws.exception.CommitException;
import br.com.fiap.ws.singleton.EntityManagerFactorySingleton;

@Path("/produto")
public class ProdutoResource {
	
	private ProdutoDAO dao;

	public ProdutoResource() {
		//Inicializar o DAO
		EntityManager em = EntityManagerFactorySingleton
				.getInstance().createEntityManager();
		
		dao = new ProdutoDAOImpl(em);
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Produto buscar(@PathParam ("id") int codigo) {
		return dao.read(codigo);
	}
	
	//Listar os produtos
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Produto> listar() {
		return dao.list();
	}
	
//	@PUT
//	@Path("/{id}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response atualizar(Produto produto, @PathParam("id") int codigo) {
//		produto.setCodigo(codigo);
//		
//		try {
//			dao.update(produto);
//			dao.commit();
//		} catch (CommitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return Response.ok().build();
//	}
	
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastrar(Produto produto, @Context UriInfo url) {
		
		try {
			dao.create(produto);
			dao.commit();
		} catch (CommitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Erro 505
			return Response.serverError().build();
		}
		//Cria a URL para acessar o recurso criado
		UriBuilder b = url.getAbsolutePathBuilder();
		b.path( String.valueOf(produto.getCodigo()));
		//201 - Created
		return Response.created(b.build()).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response atualizar(Produto produto, @PathParam("id") int codigo) {
		try {
			produto.setCodigo(codigo);
			dao.update(produto);
			dao.commit();
		} catch (CommitException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.ok().build();
	}
	
	
	@DELETE
	@Path("{id}")
	public void remover(@PathParam("id") int codigo) {
		try {
			dao.delete(codigo);
			dao.commit();
		} catch (Exception e ) {
			e.printStackTrace();
			//Exce��o com Status Code 500
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
}
