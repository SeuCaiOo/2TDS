package br.com.fiap.ws.entity;

import javax.persistence.*;

@Entity
@Table(name = "T_PRODUTO")
@SequenceGenerator(name = "produto", sequenceName = "SEQ_T_PRODUTO")
public class Produto {
	
	@Id
	@Column(name = "CD_PRODUTO")
	@GeneratedValue(generator = "produto", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name = "NM_PRODUTO", nullable = false)
	private String nome;
	
	@Column(name = "VL_PRECO")
	private double preco;
	
	@Column(name = "ST_DISPONIVEL")
	private boolean disponivel;
	
	
	

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public boolean isDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
	
	
	

}
