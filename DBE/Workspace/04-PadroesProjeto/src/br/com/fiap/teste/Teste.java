package br.com.fiap.teste;

import java.util.Properties;

import org.slf4j.*;

import br.com.fiap.singleton.PropertySingleton;

public class Teste {
	
	
	private static final Logger log = 
			LoggerFactory.getLogger(Teste.class);

	public static void main(String[] args) {

	
		
		Properties p = PropertySingleton.getInstance();
		log.trace("Iniciando o sistema");
		System.out.println(p.getProperty("url"));
		System.out.println(p.getProperty("login"));
		System.out.println(p.getProperty("senha"));
	}

}
