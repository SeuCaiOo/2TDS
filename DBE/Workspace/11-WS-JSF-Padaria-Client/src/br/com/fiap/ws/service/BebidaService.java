package br.com.fiap.ws.service;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.*;


import br.com.fiap.ws.to.Bebida;

public class BebidaService {
	
	
	private Client client = Client.create();
	private static final String URL = 
			"http://localhost:8080/11-WS-JSF-Padaria-Client/rest/bebida";

	
	/** Cadastrar Bebida */
	public void cadastrar(Bebida bebida) {
		//Cria a URL
		WebResource w = client.resource(URL);
		//Chama o WebService
		ClientResponse response = w
				.type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, bebida);
		
	}

	
	/** Atualizar Bebida */
	public void atualizar() {
		
	}
	
	/** Remover Bebida */
	public void remover() {
		
	}
	
	
	/** Listar todas Bebidas */
	public List<Bebida> listar() {
		//Cria a URL 
		WebResource w = client.resource(URL);
		//Chama o WebService
		return Arrays.asList();
	}

	/** Pesquisar uma Bebida */
	public Bebida pesquisar() {
		return null;
		
	}
	
	
	
	
}
