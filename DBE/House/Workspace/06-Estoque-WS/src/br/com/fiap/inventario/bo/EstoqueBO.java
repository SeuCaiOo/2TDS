package br.com.fiap.inventario.bo;
import java.util.*;

import org.apache.axis2.AxisFault;

import br.com.fiap.inventario.to.ProdutoTO;

public class EstoqueBO {
	//Simulacao DAO
	public List<ProdutoTO> listar() {
		List<ProdutoTO> lista = new ArrayList<>();
		lista.add(new ProdutoTO(100, 5, 401, "Camisa Masculina"));
		lista.add(new ProdutoTO(150, 1, 402, "Camisa Feminina"));
		return lista;
	}
	
	public ProdutoTO consultarProduto(int codigo) throws AxisFault {
		
		ProdutoTO to = null;
		
		switch (codigo) {
		case 401:
			to = new ProdutoTO(50, 10, 401, "Camiseta Masculina");
			break;
		case 402:
			to = new ProdutoTO(40, 15, 401, "Camiseta Feminina");
			break;
			
		default: throw new AxisFault("Produto n�o encontrado");
		}
				
		return to;
	}

}
