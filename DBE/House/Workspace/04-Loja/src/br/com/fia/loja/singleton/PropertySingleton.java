package br.com.fia.loja.singleton;

import java.io.IOException;
import java.util.Properties;


public class PropertySingleton {
	
	//1- Atributo est�tico que armazena a inst�ncia �nica
		private static Properties prop;
		
		//3- Construtor privado
		private PropertySingleton() {
			
		}
		
		//2-M�todo est�tico que retorna a inst�ncia �nica
		public static Properties getInstance() { 
			if (prop == null) {
				//Inicializa o objeto
				prop   = new Properties();
				///Carrega o objeto com as configura��es do arquivo
				try {
					prop.load(PropertySingleton.class.getResourceAsStream("/loja.properties"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 return prop;
		}

}
