package br.com.fia.loja.bo;
import br.com.fia.loja.to.ProdutoTO;

public class EstoqueBO {
	
	
	public ProdutoTO consultarProduto(int codigo) {
		
		ProdutoTO to = null;
		
		switch (codigo) {
		case 401:
			to = new ProdutoTO(50, 10, 401, "Camiseta Masculina");
			break;
		case 402:
			to = new ProdutoTO(40, 15, 401, "Camiseta Feminina");
			break;	
		}
		return to;
	}

}
