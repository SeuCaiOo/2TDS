package br.com.fia.loja.to;

import java.io.Serializable;

public class ProdutoTO implements Serializable{

	private double precoUnitario;
	private int quantidaEstoque;
	private int codido;
	private String nome;
	
	
	public ProdutoTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProdutoTO(double precoUnitario, int quantidaEstoque, int codido, String nome) {
		super();
		this.precoUnitario = precoUnitario;
		this.quantidaEstoque = quantidaEstoque;
		this.codido = codido;
		this.nome = nome;
	}
	public double getPrecoUnitario() {
		return precoUnitario;
	}
	public void setPrecoUnitario(double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}
	public int getQuantidaEstoque() {
		return quantidaEstoque;
	}
	public void setQuantidaEstoque(int quantidaEstoque) {
		this.quantidaEstoque = quantidaEstoque;
	}
	public int getCodido() {
		return codido;
	}
	public void setCodido(int codido) {
		this.codido = codido;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
}
