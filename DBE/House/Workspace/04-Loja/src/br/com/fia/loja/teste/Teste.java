package br.com.fia.loja.teste;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Scanner;

import br.com.fia.loja.bo.EstoqueBO;
import br.com.fia.loja.singleton.PropertySingleton;
import br.com.fia.loja.to.ProdutoTO;

public class Teste {
	
	public static void main(String[] args) {

		Scanner sc = new Scanner (System.in);

		EstoqueBO bo = new EstoqueBO();

		DecimalFormat df = new DecimalFormat("R$ ##,###.00");
		
		Calendar hoje = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Properties p = PropertySingleton.getInstance();

		System.out.println(p.getProperty("loja.filial")+ "\t" + sdf.format(hoje.getTime()));
		System.out.println("**************************");

		int codigo;
		double valor = 1578.6;
		
		//System.out.println(df.format(valor));
		
		//System.out.println("Produto 1: " + df.format(valor));
		
		

		do {
			System.out.print("C�digo do produto: ");
			codigo = sc.nextInt();//ler codigo
					
			if (codigo != 0 ) {
				ProdutoTO prod = bo.consultarProduto(codigo);
				
				if(prod != null) {
					System.out.println("  " + prod.getNome());
					System.out.println("  " + df.format(prod.getPrecoUnitario()));
					System.out.println("  " + prod.getQuantidaEstoque() + "\n");
				}
			}
		} 
		while(codigo != 0);
		System.out.println("Fim da Aplica��o");

		
		sc.close();
	}


}
