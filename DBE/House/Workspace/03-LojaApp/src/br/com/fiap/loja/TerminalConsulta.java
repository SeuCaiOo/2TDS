package br.com.fiap.loja;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class TerminalConsulta {

	public static void main(String[] args) {

		Scanner sc = new Scanner (System.in);

		Calendar hoje = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		System.out.println("FIAP              " + sdf.format(hoje.getTime()));
		System.out.println("****************************");

		int codigo;

		do {
			System.out.print("C�digo do produto: ");

			codigo = sc.nextInt();//ler codigo

			if (codigo == 401) {
				System.out.println("Camiseta Masculina \n");
			} else if (codigo == 402) {
				System.out.println("Camiseta Femina \n");
			}	else if (codigo == 0) {
				System.out.println("Tchau! \n");
			} else {
				System.out.println("Produto n�o cadastrado! \n");
			}
		} while(codigo != 0);
		System.out.println("Fim da Aplica��o");
		sc.close();
	}

}
