package br.com.fiap.ws.view;

import java.util.Scanner;

import br.com.fiap.ws.service.ProdutoService;
import br.com.fiap.ws.to.Produto;

public class DeleteView {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
//		Produto produto = new Produto();
		
		System.out.println("C�digo: ");
		int codigo = sc.nextInt();

		ProdutoService service = new ProdutoService();
		
		try {
			service.remover(codigo);
			System.out.println("Removido!");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		sc.close();
	}

}
