package br.com.fiap.view;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import br.com.fiap.bo.NotaBOStub;
import br.com.fiap.bo.NotaBOStub.CalcularMedia;
import br.com.fiap.bo.NotaBOStub.CalcularMediaResponse;

public class MediaView {

	public static void main(String[] args) {
		
		try {
			NotaBOStub stub = new NotaBOStub();
			CalcularMedia parametros = new CalcularMedia();
			parametros.setAm(7);
			parametros.setNac(7);
			parametros.setPs(4);
			
			
			//Chama o ws e recupera o retorno
			CalcularMediaResponse resp = stub.calcularMedia(parametros);
			
			System.out.println(resp.get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
