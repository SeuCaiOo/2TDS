package br.com.fiap.inventario.bo;
import org.apache.axis2.AxisFault;

import br.com.fiap.inventario.to.ProdutoTO;

public class EstoqueBO {
	
	
	public ProdutoTO consultarProduto(int codigo) throws AxisFault {
		
		ProdutoTO to = null;
		
		switch (codigo) {
		case 401:
			to = new ProdutoTO(50, 10, 401, "Camiseta Masculina");
			break;
		case 402:
			to = new ProdutoTO(40, 15, 401, "Camiseta Feminina");
			break;
			
		default: throw new AxisFault("Produto n�o encontrado");
		}
				
		return to;
	}

}
