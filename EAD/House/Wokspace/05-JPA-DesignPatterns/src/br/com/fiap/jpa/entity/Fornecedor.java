package br.com.fiap.jpa.entity;

import javax.persistence.*;


@Entity
@Table(name = "T_FORNECEDOR")
public class Fornecedor {
	
	@Id
	@Column(name = "NR_CNPJ")
	private String cnpj;
	
	@Column(name = "NM_NOME", nullable=false)
	private String nome;	

	public Fornecedor() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Fornecedor(String cnpj, String nome) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
	}


	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
