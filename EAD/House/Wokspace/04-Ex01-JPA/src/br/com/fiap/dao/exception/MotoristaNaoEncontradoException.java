package br.com.fiap.dao.exception;

public class MotoristaNaoEncontradoException extends Exception{

	public MotoristaNaoEncontradoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MotoristaNaoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MotoristaNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MotoristaNaoEncontradoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MotoristaNaoEncontradoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
