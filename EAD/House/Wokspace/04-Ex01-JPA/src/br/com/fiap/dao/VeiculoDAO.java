package br.com.fiap.dao;

import br.com.fiap.entity.Veiculo;

public interface VeiculoDAO {
	
	Veiculo cadastrar(Veiculo veiculo); 
	Veiculo atualizar(Veiculo veiculo);
	Veiculo pesquisar(int codigo);
	void remover(int codigo);
	
	void commit();

}
