package br.com.fiap.dao;

import br.com.fiap.entity.Motorista;

public interface MotoristaDAO {
	
	Motorista cadastrar(Motorista motorista);
	Motorista atualizar(Motorista motorista);
	Motorista pesquisar(int codigo);
	void remover(int codigo);

}
