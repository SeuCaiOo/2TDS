package br.com.fiap.entity;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "T_VEICULO")
@SequenceGenerator(name = "veiculo", sequenceName = "SQ_VEICULO")
public class Veiculo implements Serializable{
	
	@Id
	@Column(name = "CD_VEICULO")
	private double codigo;
	
	@Column(name = "DS_PLACA", nullable = false)
	private String placa;
	
	@Column(name = "DS_COR", nullable = false)
	private String cor;
	
	@Column(name = "NR_ANO")
	private double ano;
	
	
	

}
