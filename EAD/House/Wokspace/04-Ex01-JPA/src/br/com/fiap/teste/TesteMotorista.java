package br.com.fiap.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.fiap.dao.MotoristaDAO;
import br.com.fiap.dao.impl.MotoristaDAOImpl;
import br.com.fiap.entity.Genero;
import br.com.fiap.entity.Motorista;
import br.com.fiap.singleton.EntityManagerFactorySingleton;

public class TesteMotorista {

	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = EntityManagerFactorySingleton.getInstance();
		EntityManager em = fabrica.createEntityManager();
		
		MotoristaDAO dao = new MotoristaDAOImpl(em);
		
		Motorista motorista = new Motorista(001, "Caio", new GregorianCalendar(1997, Calendar.JANUARY, 23), null, Genero.MASCULINO);
		
		
		dao.cadastrar(motorista);
	
		
	}

	
	
}
