package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table (name = "T_MOTORISTA")
public class Motorista {
	
	@Id
	@Column(name = "NR_CARTEIRA" )
	private double carteira;
	
	@Column(name =  "NM_MOTORISTA", nullable= false)
	private String motorista;
	
	@Column(name = "DT_NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Calendar dtNascimento;
	
	@Lob
	@Column(name = "FL_CARTEIRA")
	private byte[] flCarteira;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "DS_GENERO")
	private Genero genero;
	
	
	
	

	public Motorista() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Motorista(double carteira, String motorista, Calendar dtNascimento, byte[] flCarteira, Genero genero) {
		super();
		this.carteira = carteira;
		this.motorista = motorista;
		this.dtNascimento = dtNascimento;
		this.flCarteira = flCarteira;
		this.genero = genero;
	}

	public double getCarteira() {
		return carteira;
	}

	public void setCarteira(double carteira) {
		this.carteira = carteira;
	}

	public String getMotorista() {
		return motorista;
	}

	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}

	public Calendar getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Calendar dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public byte[] getFlCarteira() {
		return flCarteira;
	}

	public void setFlCarteira(byte[] flCarteira) {
		this.flCarteira = flCarteira;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}
	
	
	

}
