package br.com.fiap.jpa.entity;

import javax.persistence.*;

@Entity
@Table(name= "T_ITEM_PEDIDO")
@SequenceGenerator(name= "item", sequenceName = "SEQ_T_ITEM_PEDIDO", allocationSize =1)
public class ItemPedido {
	
	@Id
	@Column(name = "CD_ITEM")
	@GeneratedValue(generator = "item", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name ="QT_ITEM")
	private int quantidade;
	
	@Column(name = "VL_ITEM")
	private double valor;
	
	@ManyToOne
	@JoinColumn(name = "CD_PEDIDO")
	private Pedido pedido;
	
	
	

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	
	
	
}
