package br.com.fiap.jpa.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.*;

import br.com.fiap.entity.Bebida;
import br.com.fiap.entity.TipoBebida;
import br.com.fiap.jpa.dao.BebidaDAO;
import br.com.fiap.jpa.dao.impl.BebidaDAOImpl;
import br.com.fiap.jpa.exception.CommitException;

public class DAOTeste {

	public static void main(String[] args) {
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("oracle");
		EntityManager em = fabrica.createEntityManager();
		
		
		BebidaDAO dao = new BebidaDAOImpl(em);
		
		//Cadastrar
		Bebida bebida = new Bebida(TipoBebida.REFRIGERANTE, "Pepsi", 
				new GregorianCalendar(2019, Calendar.JUNE, 2), null, false);
		
		try {
			dao.cadastrar(bebida);
			dao.commit();
			System.out.println("\n Cadastrado! \n");
		} catch (CommitException e) {
			System.err.println(e.getMessage());
		}
		
		//Buscar
		Bebida bebidaBusca = dao.buscar(bebida.getId());
		System.out.println(bebida.getNome() + "\n");
		
		//Atualizar
		bebidaBusca.setNome("Coca-Cola");
				
		try {
			dao.atualizar(bebidaBusca);
			dao.commit();
			System.out.println("\n Atualizado!\n ");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		//Remover
		
		try {
			dao.remover(bebidaBusca.getId());
			dao.commit();
			System.out.println("\n Removido! \n");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		em.close();
		fabrica.close();

	}

}
