package br.com.fiap.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.entity.Bebida;
import br.com.fiap.entity.TipoBebida;

public class AtualizacaoTeste {

	
	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("oracle");
		
		EntityManager em = fabrica.createEntityManager();

		
		Bebida bebida = new Bebida(TipoBebida.CERVEJA, "Kaiser",
				new GregorianCalendar(2019, Calendar.JANUARY, 2), null, true);
				//em.find(Bebida.class, 1);	
		
		//bebida.setNome("Pepsi");
		
		//bebida.setId(1);
		em.merge(bebida);
		
		em.getTransaction().begin();
		em.getTransaction().commit();
		
		System.out.println("\n \tBebida: " + bebida.getNome() + "\n");
		
		
		
		
		em.close();
		fabrica.close();
	}
}
