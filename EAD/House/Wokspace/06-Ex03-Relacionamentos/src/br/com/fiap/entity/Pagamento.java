package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name = "T_PAGAMENTO")
@SequenceGenerator(name = "pagamento", sequenceName= "SEQ_T_PAGAMENTO", allocationSize = 1)
public class Pagamento {
	
	@Id
	@Column(name = "CD_PAGAMENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int pagamento;
	
	@Column(name = "DT_PAGAMENTO", nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar data;
	
	@Column(name = "VL_PAGAMENTO", nullable = false)
	private double valor;
	
	@Column(name = "DS_FORMA_PAGAMENTO", nullable = false)
	@Enumerated(EnumType.STRING)
	private FormaPagamento formaPagamento;
	
	@OneToOne
	@JoinColumn(name = "CD_CORRIDA")
	private Corrida corrida;

	public Pagamento() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Pagamento(Calendar data, double valor, FormaPagamento formaPagamento) {
		super();
		this.data = data;
		this.valor = valor;
		this.formaPagamento = formaPagamento;
	}


	public int getPagamento() {
		return pagamento;
	}

	public void setPagamento(int pagamento) {
		this.pagamento = pagamento;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}


	public Corrida getCorrida() {
		return corrida;
	}


	public void setCorrida(Corrida corrida) {
		this.corrida = corrida;
	}
	

}
