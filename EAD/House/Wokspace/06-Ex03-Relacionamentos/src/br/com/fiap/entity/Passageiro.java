package br.com.fiap.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name= "T_PASSAGEIRO")
@SequenceGenerator(name= "passageiro", sequenceName = "SEQ_T_PASSAGEIRO", allocationSize = 1)
public class Passageiro {
	
	@Id
	@Column(name = "CD_PASSAGEIRO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int passageiro;
	
	@Column(name = "NM_PASSAGEIRO", nullable =false)
	private String nome;
	
	@Column(name = "DT_NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Calendar nascimento;
	
	@Column(name = "DS_GENERO")
	@Enumerated(EnumType.STRING)
	private Genero genero;
	
	@OneToMany(mappedBy = "passageiro")
	private List<Corrida> corridas;

	public Passageiro() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	public Passageiro(String nome, Calendar nascimento, Genero genero) {
		super();
		this.nome = nome;
		this.nascimento = nascimento;
		this.genero = genero;
	}




	public int getPassageiro() {
		return passageiro;
	}

	public void setPassageiro(int passageiro) {
		this.passageiro = passageiro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getNascimento() {
		return nascimento;
	}

	public void setNascimento(Calendar nascimento) {
		this.nascimento = nascimento;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}




	public List<Corrida> getCorridas() {
		return corridas;
	}




	public void setCorridas(List<Corrida> corridas) {
		this.corridas = corridas;
	}


	

}
