package br.com.fiap.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "T_VEICULO")
@SequenceGenerator(name = "veiculo", sequenceName = "SEQ_T_VEICULO", allocationSize = 1)
public class Veiculo implements Serializable{
	
	@Id
	@Column(name = "CD_VEICULO")
	@GeneratedValue(generator = "veiculo", strategy = GenerationType.SEQUENCE)
	private double codigo;
	
	@Column(name = "DS_PLACA", nullable = false)
	private String placa;
	
	@Column(name = "DS_COR", nullable = false)
	private String cor;
	
	@Column(name = "NR_ANO")
	private double ano;
	
	@ManyToMany(mappedBy = "veiculos")
	private List<Motorista> motoristas;
	

	public Veiculo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Veiculo(String placa, String cor, double ano) {
		super();
		this.placa = placa;
		this.cor = cor;
		this.ano = ano;
	}

	public double getCodigo() {
		return codigo;
	}

	public void setCodigo(double codigo) {
		this.codigo = codigo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public double getAno() {
		return ano;
	}

	public void setAno(double ano) {
		this.ano = ano;
	}
	
	
	

}
