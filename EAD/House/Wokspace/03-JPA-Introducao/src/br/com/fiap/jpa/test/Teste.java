package br.com.fiap.jpa.test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import br.com.fiap.jpa.entity.Carro;
import br.com.fiap.jpa.entity.Transmissao;

public class Teste {
	
	public static void main(String[] args) {
		EntityManagerFactory fabrica = 
				Persistence.createEntityManagerFactory("oracle");
		EntityManager em = fabrica.createEntityManager();
		
		Carro carro = new Carro("Jetta", 2013, "2.0 TSI", "Volkswagen", "XXX-6969",
				new GregorianCalendar(2013, Calendar.JANUARY, 2), 
				false, true, null, Transmissao.MANUAL);
		
		em.getTransaction().begin();//inicializa uma transa��o
		em.persist(carro);
		em.getTransaction().commit();// realiza o commit
		
		em.close();
		fabrica.close();
		
		
	}

}
