package br.com.fiap.tdsr.teste;

import java.awt.Window.Type;
import java.lang.reflect.Field;

import br.com.fiap.tdsr.anotacao.Tabela;
import br.com.fiap.tdsr.anotacao.ValidaVazio;
import br.com.fiap.tdsr.bean.Time;

public class AnotacaoTeste {
	
	public static void main(String[] args) {
		Time time = new Time("Noroeste", 0);
	
		//Recupera o nome da classe
		String nome = time.getClass().getName();
		System.out.println("Nome da classe: " + nome);
		
		Tabela t = time.getClass().getAnnotation(Tabela.class);
		
		System.out.println("Tabela: " + time.getClass().getName() + " > " +  t.scritp());

		
		//Recuperar os atributos
		Field[] attrs = time.getClass().getDeclaredFields();
		
	
		for (Field field : attrs) {
			//Recuperar a anota��o		
			ValidaVazio anotacao = field.getAnnotation(ValidaVazio.class);
			if(anotacao != null) {
				
				System.out.println(anotacao.mensagem());
			}

		}
			
		}
	}

