package br.com.fiap.jpa.entity;

import javax.persistence.*;

@Entity
@Table(name = "T_SORVETE")
@SequenceGenerator(name = "sorvete", 
	sequenceName = "SQ_T_SORVETE", allocationSize = 1)
public class Sorvete {
	
	@Id
	@Column(name= "CD_SORVETE")
	@GeneratedValue(generator = "sorvete", strategy=GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name = "DS_SABOR", nullable=false, length=50)
	private String sabor;
	
	@Column(name = "VL_PRECO", nullable=false)
	private double preco;	

	public Sorvete() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Sorvete(String sabor, double preco) {
		super();
		this.sabor = sabor;
		this.preco = preco;
	}



	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	
	

}
