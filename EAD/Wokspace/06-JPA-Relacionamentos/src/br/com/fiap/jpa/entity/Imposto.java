package br.com.fiap.jpa.entity;

import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "T_IMPOSTO")
@SequenceGenerator(name = "imposto", sequenceName = "SEQ_T_IMPOSTO", allocationSize = 1) 
public class Imposto {
	
	@Id
	@Column(name = "CD_IMPOSTO")
	@GeneratedValue(generator = "imposto", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name = "VL_IMPOSTO")
	private double valor;
	
	@Column(name = "NM_IMPOSTO")
	private String nome;
	
	@ManyToMany(mappedBy = "impostos")
	private List<NotaFiscal> notas;
		
	
	

	public Imposto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Imposto(double valor, String nome, List<NotaFiscal> notas) {
		super();
		this.valor = valor;
		this.nome = nome;
		this.notas = notas;
	}

	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	

}
