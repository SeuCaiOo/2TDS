package br.com.fiap.jpa.entity;

public enum StatusPedido {

	APROVATO, REPROVADO, CANELADO, PENDENTE
}
