package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table (name = "T_MOTORISTA")
public class Motorista {
	
	@Id
	@Column(name = "NR_CARTEIRA" )
	private double carteira;
	
	@Column(name =  "NM_MOTORISTA", nullable= false)
	private String motorista;
	
	@Column(name = "DT_NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Calendar dtNascimento;
	
	@Lob
	@Column(name = "FL_CARTEIRA")
	private byte[] flCarteira;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "DS_GENERO")
	private Genero genero;

}
