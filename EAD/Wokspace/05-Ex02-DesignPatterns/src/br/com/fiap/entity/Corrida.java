package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name = "T_CORRIDA")
@SequenceGenerator(name = "corrida", sequenceName = "SEQ_T_CORRIDA", allocationSize = 1)
public class Corrida {

	@Id
	@Column(name = "CD_CORRIDA")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int corrida;
	
	@Column(name = "DS_ORIGEM")
	private String origem;
	
	@Column(name = "DS_DESTINO")
	private String destino;
	
	@Column(name = "DT_CORRIDA")
	@Temporal(TemporalType.DATE)
	private Calendar data;
	
	@Column(name = "VL_CORRIDA", nullable = false)
	private double valor;

	

	public Corrida() {
		super();
		// TODO Auto-generated constructor stub
	}

		
	public Corrida(String origem, String destino, Calendar data, double valor) {
		super();
		this.origem = origem;
		this.destino = destino;
		this.data = data;
		this.valor = valor;
	}




	public int getCorrida() {
		return corrida;
	}

	public void setCorrida(int corrida) {
		this.corrida = corrida;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	

	
}
