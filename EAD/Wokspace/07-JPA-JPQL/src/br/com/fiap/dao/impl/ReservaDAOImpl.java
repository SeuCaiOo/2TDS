package br.com.fiap.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.dao.ReservaDAO;
import br.com.fiap.entity.Cliente;
import br.com.fiap.entity.Reserva;

public class ReservaDAOImpl extends GenericDAOImpl<Reserva,Integer> implements ReservaDAO {

	public ReservaDAOImpl(EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	public long contarQuantidadePorCliente(int idCliente) {
		
		return em.createQuery("select count(r) from Reserva r"
				+ " where r.cliente.id = :idCodigo", Long.class)
				.setParameter("idCodigo",idCliente)
				.getSingleResult();
	}

	//PROVAAA
	@Override
	public long contarQuatidadePorData(Calendar dtInicio, Calendar dtFim) {
		
		return em.createQuery("select count(r) from Reserva r "
				+ "where r.dataReserva between :i and :f", Long.class)
				.setParameter("i",dtInicio)
				.setParameter("f",dtFim)
				.getSingleResult();
	}

	@Override
	public List<Reserva> buscarPorCidade(String cidade) {
		return em.createNamedQuery("Reserva.porCidade", Reserva.class)
				.setParameter("quebrada", cidade)
				.getResultList();
	}

}
