package br.com.fiap.dao;

import java.util.Calendar;
import java.util.List;

import br.com.fiap.entity.Pacote;
import br.com.fiap.entity.Transporte;

public interface PacoteDAO extends GenericDAO<Pacote,Integer>{

	List<Pacote> buscarPorTransporte(Transporte transporte);
	
	//PROVA
	List<Pacote> buscarPorDatas(Calendar data1, Calendar data2);
	
	double calcularMediaPreco();
	
	//Contar a quantida de pacotes que possuem transporte
	long contarPorTransporte();
	
	//Somar os pre�os dos pacotes por per�odos de data
	//Provaaa
	double somarPrecoPacotesData(Calendar dt1 , Calendar dt2);
	
	List<Pacote> buscarPorDescricao(String descricao);
}
