package br.com.fiap.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.dao.PacoteDAO;
import br.com.fiap.entity.Pacote;
import br.com.fiap.entity.Transporte;

public class PacoteDAOImpl extends GenericDAOImpl<Pacote,Integer> implements PacoteDAO{

	public PacoteDAOImpl(EntityManager entityManager) {
		super(entityManager);
	}

	@Override
	public List<Pacote> buscarPorTransporte(Transporte transporte) {
		
		return em.createQuery("from Pacote p where p.transporte = :tr", Pacote.class)
				.setParameter("tr",transporte)
				.getResultList();
	}

	@Override
	public List<Pacote> buscarPorDatas(Calendar data1, Calendar data2) {
		
		return em.createQuery("from Pacote p where p.dataSaida between :d1 AND :d2 ", Pacote.class)
				.setParameter("d1", data1)
				.setParameter("d2", data2)
				.getResultList();
	}

	@Override
	public double calcularMediaPreco() {
		
		return em.createQuery("select avg(p.preco) from Pacote p", Double.class)
				.getSingleResult();
	}

	@Override
	public long contarPorTransporte() {
		return em.createNamedQuery("Pacote.contarPorTransporte", Long.class)
			.getSingleResult();
	}

	//Provaaaa
	@Override
	public double somarPrecoPacotesData(Calendar dt1, Calendar dt2) {
		return em.createNamedQuery("Pacote.somarPreco", Double.class)
				.setParameter("data1", dt1)
				.setParameter("data2", dt2)
				.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pacote> buscarPorDescricao(String descricao) {
		return em.createNativeQuery("select * from "
				+ "JPA_T_PACOTE where DS_PACOTE like :d", Pacote.class)
				.setParameter("d", "%"+descricao+"%")
				.getResultList();
	}

}
