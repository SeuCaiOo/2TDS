package br.com.fiap.teste;

import javax.persistence.*;

import br.com.fiap.entity.Bebida;

public class BuscaTeste {

	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("oracle");
		
		EntityManager em = fabrica.createEntityManager();

		
		Bebida bebida = em.find(Bebida.class, 1);	
		
		System.out.println("\n \tBebida: " + bebida.getNome() + "\n");
		
		em.close();
		fabrica.close();
	}

}
