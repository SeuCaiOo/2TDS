package br.com.fiap.teste;

import javax.persistence.*;

import br.com.fiap.entity.Bebida;



public class Refresh {
	
	public static void main(String[] args) {
		
//		1- Criar o EntityManager
		
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("oracle");
		EntityManager em = fabrica.createEntityManager();
		
//		2- Buscar uma bebida
		
		Bebida bebida = em.find(Bebida.class,2);
	
//		3- Exibir o nome
		
		System.out.println("\n \tBebida (Antigo Nome): " + bebida.getNome() + "\n");
		
//		4- Mudar o nome da bebida no java
		
		bebida.setNome("Corona");
		
//		5- Exibir o nome
		
		System.out.println("\n \tBebida (Novo Nome): " + bebida.getNome() + "\n");
		
//		6- Fazer o refresh
		
		em.refresh(bebida);
		
//		7- Exibir o nome

		System.out.println("\n \tBebida (Refresh Nome): " + bebida.getNome() + "\n");
		
		em.close();
		fabrica.close();
		
		
		
	}

}
