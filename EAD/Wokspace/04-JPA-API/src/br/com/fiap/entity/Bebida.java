package br.com.fiap.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

import org.hibernate.annotations.ValueGenerationType;

@Entity
@Table(name = "TB_BEBIDA" )
@SequenceGenerator(name = "bebida", sequenceName = "SQ_TB_BEBIDA", allocationSize=1)
public class Bebida implements Serializable {

	@Id
	@Column(name = "CD_BEBIDA")
	@GeneratedValue(generator = "bebida", strategy=GenerationType.SEQUENCE)
	private int id;
	
	@Column(name = "DS_TIPO")
//	@Enumerated(EnumType.STRING)
	private TipoBebida tipo;
	
	@Column(name = "NM_BEBIDA", nullable=false, length=50)
	private String nome;
	
	@Column(name = "DT_VALIDADE")
	@Temporal(TemporalType.DATE)
	private Calendar dtValidade;
	
	@Lob
	@Column(name = "FL_ROTULO")
	private byte [] rotulo;
	
	@Column(name = "FG_ALCOOLICO")
	private boolean alcoolico;
	
	

	public Bebida() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bebida(TipoBebida tipo, String nome, Calendar dtValidade, byte[] flRotulo, boolean fgAlcoolico) {
		super();
		this.tipo = tipo;
		this.nome = nome;
		this.dtValidade = dtValidade;
		this.rotulo = flRotulo;
		this.alcoolico = fgAlcoolico;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TipoBebida getTipo() {
		return tipo;
	}

	public void setTipo(TipoBebida tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDtValidade() {
		return dtValidade;
	}

	public void setDtValidade(Calendar dtValidade) {
		this.dtValidade = dtValidade;
	}

	public byte[] getFlRotulo() {
		return rotulo;
	}

	public void setFlRotulo(byte[] flRotulo) {
		this.rotulo = flRotulo;
	}

	public boolean isFgAlcoolico() {
		return alcoolico;
	}

	public void setFgAlcoolico(boolean fgAlcoolico) {
		this.alcoolico = fgAlcoolico;
	}
	
	
	
}
