package br.com.fiap.entity;

public enum TipoBebida {
	
	CERVEJA, VINHO, REFRIGERANTE, SUCO

}
