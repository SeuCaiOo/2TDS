package br.com.fiap.jpa.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name = "T_PESSOA_FISICA")

@DiscriminatorValue("PF")

public class PessoaFisica extends Pessoa{
	
	
	
	@Column(name = "NR_CPF")
	private String cpf;
	
	@Column(name = "DT_NASCIMENTO")
	private Calendar dataNascimento;
	

	public PessoaFisica() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public PessoaFisica(String nome, String endereco, String cpf, Calendar dataNascimento) {
		super(nome, endereco);
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}




	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	

}
