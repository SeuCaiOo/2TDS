package br.com.fiap.jpa.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_PESSOA_JURIDICA")

@DiscriminatorValue("PJ")

public class PessoaJuridica extends Pessoa{

	@Column(name = "NR_CNPJ")
	private String cnpj;
	
	@Column(name = "DT_FUNDACAO")
	private Calendar dataFundacao;

	
	
	
	
	public PessoaJuridica() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public PessoaJuridica(String nome, String endereco, String cnpj, Calendar dataFundacao) {
		super(nome, endereco);
		this.cnpj = cnpj;
		this.dataFundacao = dataFundacao;
	}



	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Calendar getDataFundacao() {
		return dataFundacao;
	}

	public void setDataFundacao(Calendar dataFundacao) {
		this.dataFundacao = dataFundacao;
	}
	
	
}
