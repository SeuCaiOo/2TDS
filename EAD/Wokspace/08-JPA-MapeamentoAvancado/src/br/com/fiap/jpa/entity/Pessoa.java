package br.com.fiap.jpa.entity;

import javax.persistence.*;

//@Inheritance(strategy = InheritanceType.JOINED)
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
//Configuracoes para a Single Table
@DiscriminatorColumn(name = "DS_TIPO")
@DiscriminatorValue("P")


@Entity
@Table(name = "T_PESSOA")
@SequenceGenerator(name = "pessoa", sequenceName = "SEQ_T_PESSOA", allocationSize =1)
public class Pessoa {
	
	@Id
	@Column(name = "CD_PESSOA" )
	@GeneratedValue(generator = "pessoa", strategy = GenerationType.SEQUENCE)
	private int codigo;

	@Column(name = "NM_PESSOA")
	private String nome;
	
	@Column(name = "DS_ENDERECO")
	private String endereco;
	
	
	

	public Pessoa() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

	public Pessoa(String nome, String endereco) {
		super();
		this.nome = nome;
		this.endereco = endereco;
	}




	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	

}
