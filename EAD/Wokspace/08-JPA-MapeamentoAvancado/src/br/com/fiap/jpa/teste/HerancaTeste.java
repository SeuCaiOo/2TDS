package br.com.fiap.jpa.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import br.com.fiap.jpa.dao.PessoaDAO;
import br.com.fiap.jpa.dao.impl.PessoaDAOImpl;
import br.com.fiap.jpa.entity.Pessoa;
import br.com.fiap.jpa.entity.PessoaFisica;
import br.com.fiap.jpa.entity.PessoaJuridica;
import br.com.fiap.jpa.singleton.EntityManagerFactorySingleton;

public class HerancaTeste {

	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = EntityManagerFactorySingleton.getInstance();
		EntityManager em = fabrica.createEntityManager();
		
	
	
		//Cadastrar uma pessoa, pf, pj
		PessoaDAO pessoaDAO = new PessoaDAOImpl(em);
	
		Pessoa p = new Pessoa("Caio", "S�o Paulo");
		PessoaFisica pf = new PessoaFisica("Lucas", "Rio de Janeiro", "123.465.5646-98",
				new GregorianCalendar(2014, Calendar.SEPTEMBER, 20));
		PessoaJuridica pj = new PessoaJuridica("FIAP", "Av Lins", "23.332.213/0001-56",
				new GregorianCalendar(2011, Calendar.APRIL, 5));
		
		
		try {
			pessoaDAO.create(p);
			pessoaDAO.create(pf);
			pessoaDAO.create(pj);
			pessoaDAO.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		em.close();
		fabrica.close();
	}
}
