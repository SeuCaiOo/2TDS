package br.com.fiap.jpa.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name = "T_CONTA_CORRENTE")
@IdClass(ContaCorrentePK.class)
public class ContaCorrente {

	@Id
	@Column(name = "NR_CONTA")
	private int numero;
	
	@Id
	@Column(name = "NR_AGENCIA")
	private int agencia;
	
	@Column(name = "VL_SLADO", nullable = false)
	private double saldo;
	
	@Column(name = "DT_ABERTURA")
	@Temporal(TemporalType.DATE)
	private Calendar dataAbertura;
	
	
	
	

	public ContaCorrente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ContaCorrente(int numero, int agencia, double saldo, Calendar dataAbertura) {
		super();
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
		this.dataAbertura = dataAbertura;
	}




	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Calendar getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Calendar dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	
}
