package br.com.fiap.jpa.entity;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name ="T_PACIENTE")
@SequenceGenerator(name ="paciente", sequenceName= "SEQ_T_PACIENTE", allocationSize = 1)
public class Paciente {
	
	@Id
	@Column(name = "CD_PACIENTE")
	@GeneratedValue(generator = "paciente", strategy = GenerationType.SEQUENCE)
	private int codigo;
	
	@Column(name ="NM_PACIENTE", length =150)
	private String nome;
	
	@Column(name = "DT_NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Calendar dataNascimento;
	
	

	public Paciente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Paciente(String nome, Calendar dataNascimento) {
		super();
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}

	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	
	
}
