package br.com.fiap.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Entity
@Table(name = "T_MEDICO")
@SecondaryTable(name = "T_MEDICO_FINANCEIRO")
public class Medico {
	
	@Id
	@Column(name = "NR_CRM")
	private int crm;
	
	@Column(name ="NM_MEDICO", length = 100)
	private String nome;
	
	@Column(name = "VL_SALARIO", table = "T_MEDICO_FINANCEIRO")
	private float salario;
	
	@Column(name = "NR_CONTA", table = "T_MEDICO_FINANCEIRO")
	private int conta;
	

	public Medico() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Medico(int crm, String nome, float salario, int conta) {
		super();
		this.crm = crm;
		this.nome = nome;
		this.salario = salario;
		this.conta = conta;
	}



	public int getCrm() {
		return crm;
	}

	public void setCrm(int crm) {
		this.crm = crm;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public int getConta() {
		return conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	
	
}
