package br.com.fiap.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.dao.VeiculoDAO;
import br.com.fiap.entity.Veiculo;

public class VeiculosDAOImpl implements VeiculoDAO {
	
	private EntityManager em;	

	public VeiculosDAOImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Veiculo cadastrar(Veiculo veiculo) {

		em.persist(veiculo);
		
		
		return veiculo;
	}

	@Override
	public Veiculo atualizar(Veiculo veiculo) {
		
		em.merge(veiculo);
		
		return veiculo;
	}

	@Override
	public Veiculo pesquisar(int codigo) {
	
		
		return em.find(Veiculo.class, codigo);
	}

	@Override
	public void remover(int codigo) {
	
		Veiculo veiculo = pesquisar(codigo);
		
		em.remove(codigo);
	}

	@Override
	public void commit() {
		// TODO Auto-generated method stub
		
	}

}
