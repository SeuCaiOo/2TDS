package br.com.fiap.dao.impl;

import javax.persistence.*;

import br.com.fiap.dao.MotoristaDAO;
import br.com.fiap.entity.Motorista;

public class MotoristaDAOImpl implements MotoristaDAO {

	private EntityManager em;	
	
	public MotoristaDAOImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Motorista cadastrar(Motorista motorista) {
	
		em.persist(motorista);
		
		return motorista;
	}

	@Override
	public Motorista atualizar(Motorista motorista) {
		
		em.merge(motorista);
		
		return motorista;
	}

	@Override
	public Motorista pesquisar(int codigo) {
	
		
		return em.find(Motorista.class, codigo);
	}

	@Override
	public void remover(int codigo) {
		Motorista motorista = pesquisar(codigo);
		
		
		em.remove(motorista);
		
	}

}
