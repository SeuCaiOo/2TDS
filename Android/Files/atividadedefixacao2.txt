Atividade de fixação

Desenvolva um app que exiba uma imagem de um dado e possua um botão sortear.

Ao clicar no botão sortear, a imagem do dado deverá mudar de forma aleatória.

Dica: utilize a classe Random para gerar números aleatórios de 0 a 5 (representado
os dados de 1 a 6).

As imagens se encontram no portal junto com o exercício.
