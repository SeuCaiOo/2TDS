package br.com.fiap.pickersanddialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    DatePicker dtpDataNasc;
    TimePicker tpHoraNasc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dtpDataNasc = findViewById(R.id.dtpDataNasc);
        tpHoraNasc = findViewById(R.id.tpHora);
    }

    public void salvar(View view) {

        int dia = dtpDataNasc.getDayOfMonth();
        int mes = dtpDataNasc.getDayOfMonth()+1;
        int ano = dtpDataNasc.getYear();

        int hora = tpHoraNasc.getCurrentHour();
        int minuto = tpHoraNasc.getCurrentMinute();

        String msg =String.format("%d/%d/%d - %d:%d", dia,mes,ano,hora,minuto);

        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    public void abrirAlertDialog(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Eu sou um Alert");
        alert.setMessage("Conteúdo do Alert");

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"Ciquei", Toast.LENGTH_SHORT).show();
            }
        });
        alert.setNegativeButton("Cancelar", null);

        alert.show();
    }

    public void abrirProgressDialog(View view) {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Eu sou um Progress Dialog");
        pd.setMessage("Carregando");

        pd.dismiss();
        pd.show();

    }

    public void abrirDatePickerDialog(View view) {
        DatePickerDialog dialog = new DatePickerDialog(
                this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        //Logiga de pegar informação
                        Toast.makeText(MainActivity.this, dayOfMonth
                                + "/" + (month+1) + "/" + year, Toast.LENGTH_SHORT).show();
                    }
                },
                2018,
                4,
                17
        );
        dialog.show();
    }

    public void abrirDialogPersonalizado(View view) {
        Dialog dialogCustom = new Dialog(this);
        dialogCustom.setTitle("Eu sou um Dialog Personlizado");
        dialogCustom.setContentView(R.layout.dialog_personalizado);
        dialogCustom.show();

    }
}
