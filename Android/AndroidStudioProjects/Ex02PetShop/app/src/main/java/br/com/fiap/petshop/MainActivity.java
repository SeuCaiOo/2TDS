package br.com.fiap.petshop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    CheckBox chkFemea;
    CheckBox chkAdestrado;
    CheckBox chkVacina;
    TextView txtResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.rg);
        chkFemea = findViewById(R.id.chkFemea);
        chkAdestrado = findViewById(R.id.chkAdestrado);
        chkVacina = findViewById(R.id.chkvacina);


    }

    public void calculo(View view) {


        double resultado = 00.00;

        int selecionado =radioGroup.getCheckedRadioButtonId();

        if (chkFemea.isChecked()) {
            resultado = resultado + 180.00;
        }  if (chkAdestrado.isChecked()) {
            resultado = resultado + 400.00;
        } if (chkVacina.isChecked())  {
            resultado = resultado + 200.00;
        }

        switch (selecionado) {
            case R.id.rb1:
                resultado = resultado + 800.00;
                break;
            case R.id.rb2:
                resultado = resultado + 750.00;
                break;
            case R.id.rb3:
                resultado = resultado + 700.00;
                break;
            case R.id.rb4:
                resultado = resultado + 800.00;
                break;
            default:

        }

        txtResultado.setText(String.valueOf(resultado));
    }
}
