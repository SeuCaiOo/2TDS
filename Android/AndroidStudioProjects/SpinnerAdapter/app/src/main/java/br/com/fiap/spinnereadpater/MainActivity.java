package br.com.fiap.spinnereadpater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Spinner spnPaises;
    String [] paises = {
            "Alemanha",
            "Belize",
            "Camarões",
            "Dinamarca",
            "Espanha",
            "Finlândia"
    };


    AutoCompleteTextView atcPaises;

    TextView txtPais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Precisa de um Adapter para a parte visual
        //Adaptar dados para Layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                paises
        );
        spnPaises = findViewById(R.id.spnPaises);
        spnPaises.setAdapter(adapter);


        txtPais = findViewById(R.id.txtPais);

//        spnPaises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String pais = paises[position];
//                txtPais.setText(String.valueOf(pais));
//            }
//        });

        spnPaises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String pais = paises[position];
                txtPais.setText(String.valueOf(pais));
                Toast.makeText(MainActivity.this, getString(R.string.txt_msg) + pais, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> adapterAutoComplete = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                paises
        );
        atcPaises = findViewById(R.id.atcPaises);
        //Altera o spinner pois o adapter é o mesmo
        atcPaises.setAdapter(adapter);
//        atcPaises.setAdapter(adapterAutoComplete);
    }



    public void salvar(View view) {
        String pais = spnPaises.getSelectedItem().toString();
        Toast.makeText(this,getString(R.string.txt_msg) + pais, Toast.LENGTH_SHORT).show();
    }
}
