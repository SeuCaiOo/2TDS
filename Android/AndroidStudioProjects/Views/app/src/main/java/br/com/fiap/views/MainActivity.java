package br.com.fiap.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    CheckBox chkEstouAprendendo;
    RadioGroup rdgDificuldade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chkEstouAprendendo = findViewById(R.id.chkEstouAprendendo);
        rdgDificuldade = findViewById(R.id.rdgDificuldade);

    }


    public void salvar(View view) {

        boolean checando = chkEstouAprendendo.isChecked();
        Toast.makeText(
                this,
                getString(R.string.estou_aprendendo) + checando,
                Toast.LENGTH_LONG
        ).show();

            String msg = "";
            int id = rdgDificuldade.getCheckedRadioButtonId();

            switch (id) {
                case R.id.rbdFacil:
                    msg = getString(R.string.facil);
                    break;
                case R.id.rbdMedio:
                    msg = getString(R.string.medio);
                    break;
                case R.id.rbdDificil:
                    msg = getString(R.string.dificil);
                    break;
                default:
                    msg = getString(R.string.nada);
            }
        Toast.makeText(
                this,
                msg,
                Toast.LENGTH_LONG
        ).show();
    }
}
